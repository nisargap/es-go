package main

import (
	"bytes"
	"context"
	"encoding/json"
	"github.com/elastic/go-elasticsearch/v7"
	"github.com/elastic/go-elasticsearch/v7/esapi"
	"log"
)

type ESClient struct {
	cl *elasticsearch.Client
}

type SongDocument struct {
	Title string `json:"title"`
	Artist string `json:"artist"`
	Year int `json:"year"`
	RecordLabel string `json:"record_label"`
	Album string `json:"album"`
}

func (e *ESClient) encodeMapping() []byte {
	mapping := map[string]interface{}{
		"mappings": map[string]interface{}{
			"properties": map[string]interface{}{
				"title": map[string]interface{}{
					"type": "text",
				},
				"artist": map[string]interface{}{
					"type": "text",
				},
				"year": map[string]interface{}{
					"type": "integer",
				},
				"record_label": map[string]interface{}{
					"type": "text",
				},
				"album": map[string]interface{}{
					"type": "text",
				},
			},
		},
	}
	bs, err := json.Marshal(mapping)
	if err != nil {
		log.Fatal("could not marshal", err)
	}
	return bs
}

func (e *ESClient) CreateMusicIndex() {
	bs := e.encodeMapping()
	creq := esapi.IndicesCreateRequest{
		Index:               "musiclib",
		Body:                bytes.NewReader(bs),
		IncludeTypeName:     nil,
	}
	res, err := creq.Do(context.Background(), e.cl)
	if err != nil {
		log.Fatal("could not create index", err)
	}
	log.Println(res)
}

func (e *ESClient) encodeTitleSearchQuery(title string) []byte {
	q := map[string]interface{}{
		"query": map[string]interface{}{
			"match": map[string]interface{}{
				"title": title,
			},
		},
	}
	bs, err := json.Marshal(q)
	if err != nil {
		log.Println("error encoding search query", err)
	}
	return bs
}
func (e *ESClient) SearchBySongTitle(title string) {
	res, err := e.cl.Search(
		e.cl.Search.WithContext(context.Background()),
		e.cl.Search.WithIndex("musiclib"),
		e.cl.Search.WithBody(bytes.NewReader(e.encodeTitleSearchQuery(title))),
		e.cl.Search.WithTrackTotalHits(true),
		)
	if err != nil {
		log.Fatal("error searching", err)
	}
	// You can now handle this response however you'd like
	log.Println(res)
}

func (e *ESClient) IndexSongDocument(songDoc SongDocument) {
	song, err := json.Marshal(songDoc)
	if err != nil {
		log.Fatal("failed to encode song document to json for indexing", err)
	}
	ireq := esapi.IndexRequest{
		Index: "musiclib",
		Body: bytes.NewReader(song),
	}
	res, err := ireq.Do(context.Background(), e.cl)
	if err != nil {
		log.Fatal("failed to index song document", err)
	}
	log.Println(res)
}

func makeClient() *elasticsearch.Client {
	cfg := elasticsearch.Config{
		Addresses: []string{
			"http://localhost:9200",
		},
	}
	cl, err := elasticsearch.NewClient(cfg)
	if err != nil {
		log.Fatal("could not create an ElasticSearch client", err)
	}
	return cl
}

func NewESClient() *ESClient {
	cl := makeClient()
	return &ESClient{
		cl: cl,
	}
}

func main() {
	esCl := NewESClient()
	esCl.CreateMusicIndex()
	esCl.IndexSongDocument(SongDocument{
		"Baby",
		"Justin Bieber",
		2009,
		"Island, RBMG",
		"My World 2.0",
	})
	esCl.SearchBySongTitle("Baby")
}